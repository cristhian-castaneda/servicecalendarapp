package fr.bonjourmadame.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class CalendarBean {

    public String url;
    public Integer day;
    public String name_of_day;
    public Integer month;
    public String name_of_month;
    public Integer year;
    public String alternative_information;

}
