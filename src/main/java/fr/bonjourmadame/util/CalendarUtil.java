package fr.bonjourmadame.util;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;

@Component("CalendarUtil")
public class CalendarUtil {

    public int getDay() {
        return LocalDate.now().getDayOfMonth();
    }

    public String getDayName() {
        return LocalDate.now().getDayOfWeek().getDisplayName(TextStyle.FULL, new Locale("es", "MX"));
    }

    public int getMonth() {
        return LocalDate.now().getMonthValue();
    }

    public String getMonthName() {
        return LocalDate.now().getMonth().getDisplayName(TextStyle.FULL, new Locale("es", "MX"));
    }

    public int getYear() {
        return  LocalDate.now().getYear();
    }
}
