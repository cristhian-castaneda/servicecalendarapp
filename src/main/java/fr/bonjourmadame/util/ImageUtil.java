package fr.bonjourmadame.util;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

@Component("ImageUtil")
@Slf4j
@PropertySource("classpath:properties/calendar.properties")
public class ImageUtil {

    @Value("${app.url.page}")
    private String url;

    @Value("${app.url.source}")
    private String source;

    @Value("${app.url.default}")
    private String default_url;

    @Value("${app.url.page.default}")
    private String default_url_page;

    public String getImage() {
        String imageUrl = null;
        try {
            Document doc = Jsoup.connect(url).get();
            Elements images = doc.select("img");
            for (Element el : images) {
                String image_source = el.attr("src");
                if (image_source.contains(source) && image_source.contains(".jpg")) {
                    imageUrl = image_source;
                    log.info("Principal filter of image: " + imageUrl);
                }
            }
        } catch (IOException e) {
            log.error(e.toString());
        }
        return imageUrl == null ? getDefaultImage() : (validateSizeImage(imageUrl) ? imageUrl : getDefaultImage());
    }

    public String getDefaultImage() {
        String imageUrl = null;
        try {
            Document doc = Jsoup.connect(default_url_page).get();
            Elements images = doc.select("img");
            if (!images.isEmpty()) {
                int attempts = 0;
                while (true) {
                    String image_source = images.get(new Random().nextInt(images.size())).attr("src");
                    if (image_source.contains(default_url_page) && (image_source.contains(".jpg") || image_source.contains(".png"))) {
                        imageUrl = image_source.replace("200_", "");
                        log.info("Default filter of image: " + imageUrl);
                        if (validateSizeImage(imageUrl)) {
                            break;
                        } else if (attempts == 3) {
                            imageUrl = null;
                            break;
                        }
                        attempts++;
                    }
                }
            }
        } catch (IOException e) {
            log.error(e.toString());
        } catch (ArrayIndexOutOfBoundsException e) {
            log.error(e.toString());
        }
        return imageUrl == null ? default_url : imageUrl;
    }

    private boolean validateSizeImage(String image_source) {
        log.info("Image validation is performed");
        try {
            URL url = new URL(image_source);
            URLConnection conn = url.openConnection();
            InputStream in = conn.getInputStream();
            BufferedImage image = ImageIO.read(in);
            log.info("Height of image is: " + image.getHeight());
            if (image.getHeight() > 650)
                return true;
            else
                return false;
        } catch (Throwable e) {
            log.error(e.toString());
            return false;
        }
    }
}
