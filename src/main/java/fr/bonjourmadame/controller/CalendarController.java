package fr.bonjourmadame.controller;


import fr.bonjourmadame.bean.CalendarBean;
import fr.bonjourmadame.service.CalendarService;
import fr.bonjourmadame.util.CalendarUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController("CalendarController")
public class CalendarController {

    @Autowired
    private CalendarUtil calendarUtil;

    @Autowired
    private CalendarService calendarService;

    @RequestMapping(path = "/services/information/today", method = RequestMethod.GET)
    public ResponseEntity<CalendarBean> information() {
        CalendarBean calendarBean = new CalendarBean();
        calendarBean.setUrl(calendarService.getCurrentImage());
        calendarBean.setDay(calendarUtil.getDay());
        calendarBean.setName_of_day(calendarUtil.getDayName());
        calendarBean.setMonth(calendarUtil.getMonth());
        calendarBean.setName_of_month(calendarUtil.getMonthName());
        calendarBean.setYear(calendarUtil.getYear());
        log.info("Response: " + calendarBean.toString());
        return new ResponseEntity<CalendarBean>(calendarBean, HttpStatus.OK);
    }
}
