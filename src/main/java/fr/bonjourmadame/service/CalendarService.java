package fr.bonjourmadame.service;

import fr.bonjourmadame.util.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("CalendarService")
public class CalendarService {

    @Autowired
    private ImageUtil imageUtil;

    public String getCurrentImage() {
        return imageUtil.getImage();
    }
}
